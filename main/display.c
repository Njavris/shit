#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#include "display.h"
#include "tft.h"

#define DB0	38
#define DB1	37
#define DB2	36
#define DB3	35
#define DB4	34
#define DB5	33
#define DB6	21
#define DB7	20
#define RD	39
#define WR	40
#define CS	41
#define DC	42
#define RST	45

int db[] = {
[0] = DB0,
[1] = DB1,
[2] = DB2,
[3] = DB3,
[4] = DB4,
[5] = DB5,
[6] = DB6,
[7] = DB7
};

static void tft_write(uint8_t data) {
	gpio_set_level(RD, 1);
	gpio_set_level(WR, 0);
	for (int i = 0; i < 8; i++)
		gpio_set_level(db[i], (data >> i) & 1);
	gpio_set_level(WR, 1);
}

static void tft_write_start(int cmd) {
	for (int i = 0; i < 8; i++)
		gpio_set_direction(db[i], GPIO_MODE_OUTPUT);
	gpio_set_level(RD, 1);
	gpio_set_level(WR, 1);
	gpio_set_level(DC, !!cmd);
	gpio_set_level(CS, 0);
}

static void tft_write_end(void) {
	gpio_set_level(CS, 1);
}

static void tft_cmd(uint8_t val) {
	tft_write_start(0);
	tft_write(val);
	tft_write_end();
}

static void tft_write_8(uint8_t val) {
	tft_write_start(1);
	tft_write(val);
	tft_write_end();
}

static void tft_write_16(uint16_t val) {
	uint8_t *data = (uint8_t *)&val;
	tft_write_start(1);
	tft_write(data[1]);
	tft_write(data[0]);
	tft_write_end();
}

static void tft_write_32(uint32_t val) {
	uint8_t *data = (uint8_t *)&val;
	tft_write_start(1);
	tft_write(data[3]);
	tft_write(data[2]);
	tft_write(data[1]);
	tft_write(data[0]);
	tft_write_end();
}

static void tft_read_start(uint8_t cmd) {
	for (int i = 0; i < 8; i++)
		gpio_set_direction(db[i], GPIO_MODE_INPUT);
	gpio_set_level(CS, 0);
	gpio_set_level(DC, 0);
	tft_write(cmd);
	gpio_set_level(DC, 1);
}

static void tft_read_end(void) {
	gpio_set_level(CS, 1);
}

static uint8_t tft_read(void) {
	uint8_t ret = 0;
	gpio_set_level(RD, 0);
	gpio_set_level(WR, 1);
	for (int i = 0; i < 8; i++)
		ret |=(gpio_get_level(db[i]) & 1) << i;
	gpio_set_level(RD, 1);
	return ret;
}

__attribute__((unused)) static uint8_t tft_read_8(uint8_t cmd) {
	uint8_t ret;
	tft_read_start(cmd);
	ret = tft_read();
	tft_read_end();
	return ret;
}

__attribute__((unused)) static uint16_t tft_read_16(uint8_t cmd) {
	uint16_t ret;
	tft_read_start(cmd);
	ret = tft_read() & 0xff;
	ret |= (tft_read() & 0xff) << 8;
	tft_read_end();
	return ret;
}

__attribute__((unused)) static uint32_t tft_read_32(uint8_t cmd) {
	uint32_t ret;
	tft_read_start(cmd);
	ret = tft_read() & 0xff;
	ret |= (tft_read() & 0xff) << 8;
	ret |= (tft_read() & 0xff) << 16;
	ret |= (tft_read() & 0xff) << 24;
	tft_read_end();
	return ret;
}

void mdelay(int ms) {
	vTaskDelay(ms / portTICK_PERIOD_MS);
}

uint32_t get_display_ID(void) {
	uint32_t ret = tft_read_32(0x4);
	return ret;
}

static void tft_fill_rect(int x0, int x1, int y0, int y1, uint16_t val) {
	tft_cmd(TFT_CASET);
	tft_write_16(x0 & 0xffff);
	tft_write_16(x1 & 0xffff);
	tft_cmd(TFT_PASET);
	tft_write_16(y0 & 0xffff);
	tft_write_16(y1 & 0xffff);

	tft_write_start(0);
	tft_write(TFT_RAMWR);
	gpio_set_level(DC, 1);
	for (int i = 0; i < ((x1 - x0) * (y1 - y0)); i++) {
		tft_write((val >> 8) & 0xff);
		tft_write((val >> 0) & 0xff);
	}
	tft_write_end();
}

static void tft_draw_image(int x0, int x1, int y0, int y1, uint16_t *src) {
	tft_cmd(TFT_CASET);
	tft_write_16(x0 & 0xffff);
	tft_write_16(x1 & 0xffff);
	tft_cmd(TFT_PASET);
	tft_write_16(y0 & 0xffff);
	tft_write_16(y1 & 0xffff);

	tft_write_start(0);
	tft_write(TFT_RAMWR);
	gpio_set_level(DC, 1);
	for (int i = 0; i < ((x1 - x0) * (y1 - y0)); i++) {
		tft_write((src[i] >> 8) & 0xff);
		tft_write((src[i] >> 0) & 0xff);
	}
	tft_write_end();
}

void display_init_seq(void) {
	gpio_set_level(RST, 1);
	mdelay(10);
	gpio_set_level(RST, 0);
	mdelay(10);
	gpio_set_level(RST, 1);

	tft_cmd(TFT_SWRST);
	mdelay(150);

	tft_cmd(TFT_SLPOUT);
	mdelay(100);
	tft_cmd(TFT_COLMOD);
	tft_write_8(0x5); // RGB565
	mdelay(10);
	tft_cmd(TFT_MADCTL);
	tft_write_8(0x0);
	tft_cmd(TFT_CASET);
	tft_write_32(0x0);
	tft_cmd(TFT_PASET);
	tft_write_32(0x0);
	tft_cmd(TFT_INVON);
	mdelay(10);
	tft_cmd(TFT_NORON);
	mdelay(10);
	tft_cmd(TFT_DISPON);
	mdelay(100);
}

const uint16_t doge[115200] = {
	#include "doge.c"
};

void display_stuff(void) {
	uint32_t val;
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_OUTPUT;
	io_conf.pin_bit_mask = (1ULL << RD) | (1ULL << WR) | (1ULL << CS) |
				(1ULL << DC) | (1ULL << RST);
	io_conf.pull_down_en = 0;
	io_conf.pull_up_en = 0;
	gpio_config(&io_conf);

	io_conf.pin_bit_mask = (1ULL << DB0) | (1ULL << DB1) |
			(1ULL << DB2) | (1ULL << DB3) | (1ULL << DB4) |
			(1ULL << DB5) | (1ULL << DB6) | (1ULL << DB7);
	io_conf.mode = GPIO_MODE_INPUT;
	io_conf.pull_up_en = 0;
	gpio_config(&io_conf);

	gpio_set_level(RD, 1);
	gpio_set_level(WR, 1);
	gpio_set_level(CS, 1);
	gpio_set_level(DC, 1);
	gpio_set_level(CS, 1);

	display_init_seq();

	val = get_display_ID();
	printf("Display ID %08x\n", val);
	for (int i = 0; i < 16; i++) {
		printf("bit %d\n", i);
		tft_fill_rect(0, 240, 0, 240,  1 << i);
		vTaskDelay(10 / portTICK_PERIOD_MS);
	}
	tft_draw_image(0, 240, 0, 240, (uint16_t *)doge);
	vTaskDelay(1000 / portTICK_PERIOD_MS);
}
