#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

#include "display.h"

#ifdef DEBUG
#define debug	printf
#else
#define	debug(...)
#endif

#define PIN_NUM_MISO 8
#define PIN_NUM_MOSI 7
#define PIN_NUM_CLK  5
#define PIN_NUM_CS   6



void spi_tx(spi_device_handle_t spi, uint8_t cmd, uint16_t data) {
	spi_transaction_t trans;
	uint16_t value = data | (cmd << 12);
	uint8_t *p = (uint8_t *)&value;

	memset(&trans, 0, sizeof(spi_transaction_t));
	trans.flags = SPI_TRANS_USE_TXDATA;
	trans.length = 16;
	trans.tx_data[0] = p[0];
	trans.tx_data[1] = p[1];

	spi_device_transmit(spi, &trans);
}

void app_main(void) {
	display_stuff();
	spi_device_handle_t spi;
	spi_bus_config_t spi_bus_cfg={
		.miso_io_num=PIN_NUM_MISO,
		.mosi_io_num=PIN_NUM_MOSI,
		.sclk_io_num=PIN_NUM_CLK,
		.quadwp_io_num=-1,
		.quadhd_io_num=-1,
		.max_transfer_sz=0,
	};
	spi_device_interface_config_t spi_if_cfg={
		.clock_speed_hz=1000000,
		.command_bits = 0,
		.address_bits = 0,
		.dummy_bits = 0,
		.mode = 0,
		.duty_cycle_pos = 0,
		.cs_ena_pretrans = 0,
		.cs_ena_posttrans = 0,
		.spics_io_num=PIN_NUM_CS,
		.queue_size=24,
	};
	spi_bus_initialize(HSPI_HOST, &spi_bus_cfg, 0); 
	spi_bus_add_device(HSPI_HOST, &spi_if_cfg, &spi);

	spi_tx(spi, 0x5, 0x67);
	spi_tx(spi, 0x3, 0x880);
	spi_tx(spi, 0x7, 0xa7);
	while (1) {
		vTaskDelay(10 / portTICK_PERIOD_MS);
		for (int i = 0; i < 0xfff; i++) {
			spi_tx(spi, 0x0, i);
		}
		for (int i = 0; i < 0xfff; i++) {
			spi_tx(spi, 0x0, 0xfff - i);
		}
	}
}
